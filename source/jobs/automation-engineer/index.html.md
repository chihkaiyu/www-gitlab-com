---
layout: job_page
title: "Automation Engineer"
---

GitLab is looking for a motivated and experienced engineer to help grow our test automation efforts across the entire GitLab ecosystem. This is a key position with a new and growing team, so your efforts will have a noticeable impact to both the company and product. In addition to the requirements below, successful candidates will demonstrate a passion for high quality software, strong engineering principles and methodical problem solving skills.

## Responsibilities

- Expand our existing test automation framework
- Develop new tests and tools for our GitLab.com frontend, backend APIs and services, and low-level systems like geo replication, CI/CD, and load balancing
- Work with the product team and other development teams to understand how new features should be tested, and then engage them in contributing automated tests
- Drive adoption of best practices in code health, testing, testability and maintainability. You should know about clean code, the test pyramid and champion these concepts.
- Analyze complex software systems and collaborate with others to improve the overall design, testability and quality.
- Ensure that automated tests execute reliably and efficiently in CI/CD environments.
- Ensure test results are tracked and communicated in a timely and effective manner

## Requirements

- Strong experience developing in Ruby
- Strong experience using Git
- Experience with test automation tools like Capybara, Selenium
- Relevant internship or work experience in software development and/or test automation
- Experience working with Docker containers
- Experience with AWS or Kubernetes
- Experience with Continuous Integration systems (e.g., Jenkins, Travis, GitLab)

## Hiring Process

Applicants for this position can expect the hiring process to follow the order below. Please keep in mind that applicants can be declined from the position at any stage of the process. To learn more about someone who may be conducting the interview, find her/his job title on our [team page](https://about.gitlab.com/team/).

- Qualified applicants receive a short questionnaire from our Recruiting team
- Selected candidates will be invited to schedule a 30 minute screening call with our Recruiting team
- Next, candidates will be invited to schedule a 45 minute first interview with the Director of Quality
- Candidates will then be invited to schedule a 1 hour technical interview with the Edge Team Lead
- Candidates will be invited to schedule a third 45 minute interview with our VP of Engineering
- Finally, candidates will schedule a 50 minute interview with our CEO
- Successful candidates will subsequently be made an offer via email

Additional details about our process can be found on our [hiring page](https://about.gitlab.com/handbook/hiring/interviewing).

## Apply

Please note that if we are actively hiring for a position, you will see it listed on our [jobs page](https://about.gitlab.com/jobs/), where all of our current openings are advertised. To apply, please click on the name of the role you are interested in, which will take you to our applicant tracking system (ATS), [Lever](https://www.lever.co/).

Avoid the [confidence gap](https://www.theatlantic.com/magazine/archive/2014/05/the-confidence-gap/359815/
); you do not have to match all the listed requirements exactly to apply. Our hiring process is described in more detail in our [hiring handbook](https://about.gitlab.com/handbook/hiring/).

## About GitLab

GitLab Inc. is a company based on the GitLab open-source project. GitLab is a community project to which over 1,000 people worldwide have contributed. We are an active participant in this community, trying to serve its needs and lead by example. We have one [vision](https://about.gitlab.com/strategy/): everyone can contribute to all digital content, and our mission is to change all creative work from read-only to read-write so that everyone can contribute.

We [value](https://about.gitlab.com/handbook/values/) results, transparency, sharing, freedom, efficiency, frugality, collaboration, directness, kindness, diversity, boring solutions, and quirkiness. If these values match your personality, work ethic, and personal goals, we encourage you to visit our [primer](https://about.gitlab.com/primer/) to learn more. Open source is our culture, our way of life, our story, and what makes us truly unique.

Top 10 reasons to work for GitLab:

1. Work with helpful, kind, motivated, and talented people.
1. Work remote so you have no commute and are free to travel and move.
1. Have flexible work hours so you are there for other people and free to plan the day how you like.
1. Everyone works remote, but you don't feel remote. We don't have a head office, so you're not in a satellite office.
1. Work on open source software so you can interact with a large community and can show your work.
1. Work on a product you use every day: we drink our own wine.
1. Work on a product used by lots of people that care about what you do.
1. As a company we contribute more than we take, most of our work is released as the open source GitLab CE.
1. Focused on results, not on long hours, so that you can have a life and don't burn out.
1. Open internal processes: know what you're getting in to and be assured we're thoughtful and effective.

See [our culture page](https://about.gitlab.com/culture) for more!

Work remotely from anywhere in the world. Curious to see what that looks like? Check out our [remote manifesto](https://about.gitlab.com/2015/04/08/the-remote-manifesto/).

