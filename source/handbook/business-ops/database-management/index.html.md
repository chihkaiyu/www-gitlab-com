---
layout: markdown_page
title: "Business Operations - Database Management"
---
<a name="topofpage"></a>
## On this page
{:.no_toc}

- TOC
{:toc}  


## Lead/Contact Management   
We follow the steps detailed in the [demand waterfall](/handbook/business-ops/customer-lifecycle/#demand-waterfall-) to track the different milestones prospective customers go through as they learn about GitLab and interact with our sales and marketing teams.   

### MQL Definition  
A Marketo Qualified Lead is a lead that has reached a certain threshold, we have determined to be 90 points, based on demographic, firmographic and/or behavioral information. The [MQL score](https://docs.google.com/a/gitlab.com/document/d/1_kSxYGlIZNNyROLda7hieKsrbVtahd-RP6lU6y9gAIk/edit?usp=sharing) is comprised of various actions and/or profile data that are weighted with positive or negative point values.  
Any record with a `Person Score` greater than or equal to 50 points will be inserted into the routing flow. Using LeanData every time a `Person Score` is updated, LeanData will run a check to see if the record needs to be processed through the flow.    

### Segmentation  

#### Size   
Strategic = 5000+ users  
Large = 751-4999 users   
Mid-Market = 101-750 users   
Small-Medium Business (SMB) = 1-100 users   

#### How Segments Are Determined

Sales Segmentation can be determined by a few different criteria in Salesforce:

On the lead object, it can be defined by the following fields:
1. `Number of Developers Bucket` field, which is captured on various web forms including EE trial signups
1. `How Many Developers Do You Have?` qualifying question, which is asked during the qualification process

On the lead object, the Sales Segmentation will be determined by the larger of the two values. So if the `Number of Developers` field is `>100` but the `How Many Developers Do You Have?` field is `200`, the lead record will be categorized as `Mid-Market`.

On the account object, it can be defined by the following fields:
1. `How Many Developers Do You Have?` field, which is populated on the opportunity object after lead conversion
1. `IT + TEDD employees` The sum of IT and [TEDD](https://discoverorg.com/discoverorg-sales-intelligence-platform/data-sets-for-marketing-sales-and-staffing/technology-engineering-design-and-development-tedd-dataset/) employees from DiscoverOrg.
1. `Number of Licenses` field, which is populated from Zuora and captures the number of licenses that the customer has purchased
1. Manual override of the `Potential EE Users` field. If you decide to overwrite the `Potential EE Users` field, please note that it must be more than the `Number of Licenses` since a customer cannot have less potential users than they purchased.

Note that the default value for leads and accounts where all of the controlling fields are `NULL` or `0` is `Unknown`.

#### Parent Sales Segmentation

In addition to the current account Sales Segmentation, the Parent Sales Segmentation Field is displayed on the account record as well. The purpose of this field is to show the potential users of the parent account, as it is possible that the current account you are viewing has a potential user count of 100, for example- as it may be a specific location, business unit, or department- but that the parent account may have potential users numbering in the thousands. Please note that the child accounts' Potential EE Users do not roll up to the parent account.

#### Region/Vertical   
**Asia Pacific ([APAC](https://docs.google.com/document/d/1Ar0Y49XF0pnvWjhr5-jr0MNKdxfRY2FkS4x9y7KCZw8/edit#))**: Regional Director - Michael Alessio   
**Europe, Middle East and Africa ([EMEA](http://genesisworld.com/assets/uploads/2014/11/map_EMEA.jpg))**: Regional Director - Richard Pidgeon   
**North America - [US East](https://dev.gitlab.org/gitlab/salesforce/issues/118)**: Regional Director - Mark Rogge  
**North America - [US West](https://dev.gitlab.org/gitlab/salesforce/issues/118)**: Regional Director - Haydn Mackay  
**Public Sector**: Director of Federal Sales - Paul Almeida   

In Marketo we have modified the country values to match Salesforce to allow for seamless syncronization between the two systems. When creating new forms in Marketo, you can use the [Master Country List](https://docs.google.com/spreadsheets/d/1alYxxqMIwFeT_NWGx44Ln0pVbVTk7YIlW6AwUhxRLPQ/edit#gid=1836989764), copying column A into the form builder. 

### Initial Source  
`Initial Source` is first "known" touch attribution or when a website visitor becomes a known name in our database, it should never be changed or overwritten. If merging records, keep the `Initial Source` that is oldest (or set first). When creating Lead/Contact records and you are unsure what `Initial Source` should be used, ask in the #Lead-Questions Slack channel.   

### Lead & Contact Statuses
The Lead & Contact objects in Salesforce have unified statuses with the following definitions. If you have questions about current status, please ask in #Lead-Questions channel on Slack.

| Status | Definition |
| :--- | :--- |
| Raw | Untouched brand new lead |
| Inquiry | Form submission, meeting @ trade show, content offer |
| MQL | Marketo Qualified through systematic means |
| Accepted | Actively working to get intouch with the lead/contact |
| Qualifying | In 2-way conversation with lead/contact |
| Qualified | Progressing to next step of sales funnel (typically OPP created & hand off to Sales team) |
| Unqualified | Contact information is not now or ever valid in future; Spam form fill-out |
| Nurture | Record is not ready for our services or buying conversation now, possibly later |
| Bad Data | Incorrect data - to potentially be researched to find correct data to contact by other means |
| Web Portal Purchase | Used when lead/contact completed a purchase through self-serve channel & duplicate record exists |


### Lead Routing
The lead routing rules are determined by the incoming source (`EE Trial`, `Contact Us`, etc) and factors related to `Sales Segmentation` as well as `Region`.

#### Contact Us Requests     
All `Contact Us` requests must be followed up within **one (1) business day** (service level agreement SLA) and that follow up must be tracked as an activity on the record within Salesforce.

`Strategic`, `Large` and `Mid-Market` Sales Segments: Routed directly to the Strategic Account Leader (SAL) or Account Executive (AE) that owns the account in Salesforce.  
- At the Account Owner's discretion, they may reassign `Contact Us` follow up to an SDR, BDR or may follow-up directly.   
- Account Owner is responsible to ensure communication happens internally and externally within the SLA timeframe  
- Account Owner is responsible to user the same method of determining `Sales Qualified Amount` so it does not impact our planning in the Revenue Model. This means setting the amount to reflect the number of seats the prospect initially inquires about not the amount that will be reflected in the initial order.   

`SMB` or `Unknown` Sales Segments: Routed to the Business Development Representatives (BDR) based on region (EMEA, NCSA, APAC)  

Federal, Government, Non-Profit or Education Segments: Routed to the Federal Accounts team for follow up and response  


#### Enterprise Trial Requests   
Enterprise Edition (EE) trials can be requested through [web form](https://about.gitlab.com/free-trial/) or in-product request and default length is thirty (30) days. Trial leads are routed to the inbound BDR team based on region, regardless of sales segment. 
Regional distribution of EE Trial leads are:   
   - APAC: If Korean, route to Conor Brady; all others US/APAC BDR Round Robin  
   - EMEA: EMEA BDR Round Robin  
   - NCSA: US/APAC BDR Round Robin  
   - Federal, Government, Non-Profit, Education: US/APAC BDR Round Robin




## Rules of Engagement  

Inbound BDRs mainly work within the LEAD object in Salesforce. These records are the result of the following activities:   
- Enterprise Trial request (either web form or in product) all Sales Segments  
- EE Live Demo Webcast all Sales Segments
- Contact Request in `SMB` or `Unknown` sales segment  
- Engage in `Web Chat` through the Drift bot on select pages

Outbound SDRs mainly work within the CONTACT & ACCOUNT objects in Salesforce. These records are the result of the following activities:   
- Conference Scanned or Field Event Leads   
- CE Usage Ping data  
- List Imports from DiscoverOrg associated with their named accounts     
 
**Directionally**: If someone is exhibiting bonafide interest in evaluating EE that person is routed to an inbound BDR and is handled with the existing qualification process.  
If someone has shown interest in educational topics like high level webinars, white papers, or visited us at a tradeshow booth AND they are from a `Large` or `Strategic` account these are handled by the outbound SDR for followup.   

### Active vs. Passive

`Initial Source` cannot be used to determine if a lead is 'active' or 'passive' since the Initial Source is set upon first touch attribution; therefore, looking at the `Last Interesting Moment` field is the primary field used to begin determining if a record is actively being worked. Reviewing the `Activity History` in Salesforce is another factor considered when evaluating 'active' or 'passive'.  

A LEAD or CONTACT is considered 'Active' if they have taken an `EE Trial`, attended an EEP Demo webcast and/or engaged `Web Chat`, these are all handraising 'active' activities. These types of records are worked by the inbound BDR team. The record is considered 'Active' for entire duration of EE trial, plus 30 days after `EE Trial End Date`.   

Situational Handling of Leads  
- If a LEAD completes an **inbound** action, matches an owned ACCOUNT and there is an **open opportunity**, the BDR needs to make the Account Owner aware BEFORE outreach to the LEAD and either coordinate message or hand the LEAD off to the Account Owner for follow up.     
- If a LEAD completes and **inbound** action, matches an owned ACCOUNT, there is **not** an open opportunity, the LEAD matches a known CONTACT on the account and there is outbound activity recorded within the past 60 days, the BDR needs to reach out to the Account Owner and handoff the LEAD. 
- If a LEAD completes an **inbound** action, matches an owned ACCOUNT, there is **not** an open opportunity, the LEAD does not match any known CONTACT on the account, and there is no outbound activity recorded on the account in the past 60 days, the BDR complete standard qualification outreach to LEAD.   
- If a LEAD takes an `EE Trial` and then completes a `Contact Request`, the inbound BDR who is working the `EE Trial` retains ownership of the LEAD record, regardless of Sales Segment.  
  


## Record Ownership   
Contacts on accounts owned by a member of the Field Sales Team (RD/AE/AM/SAL), will be owned by the named Account Owner (i.e both the Account and Contact ownership will match). When an SDR is assigned to an Account to support and assist with outreach, the SDR will be added to the `GitLab Team` on the Account object within SFDC, which then populates down to the related Contact records.    

Records, both `Lead` and `Contact`, need to be synced to Outreach to ensure email activity is properly mapped back to `Activity History` in SFDC. The owner of the record in SFDC **does not** need to match the owner in Outreach. 




## Types of Accounts
### Accounts Created in Salesforce utilizing CE Usage Ping Data   
The [CE Usage ping](https://docs.gitlab.com/ee/user/admin_area/settings/usage_statistics.html) provides GitLab with some limited insight into how end users are utilizing the platform. The raw information is cleaned, enriched and then pushed to SFDC as an Account by the BizOPS team.  

If there is not an existing account match in Salesforce, a new account record will be created with the following information populated:    

| SFDC Field | Default Value |
|---|---|
| Account Name |  |
| Number of Employees |  |
| Billing Street |  |
| Billing City |  |
| Billing Zip |  |
| Billing Country |  |
| Account Type | `Prospect - CE User` |
| Account Website |  |
| Industry | Populated by Clearbit |
| Active CE Users | Populated by Usage Ping |
| CE Instances | Populated by Usage Ping |
| Account Ownwer | Sales Admin by Default |
| Using CE | Checked True |



**Process**  
1. Sales Team members can use this data to proactively identify `Prospect - CE User` accounts that fit their target segement(s). Accounts owned by `Sales Admin` can be adopted by a Sales Team member changing ownership in Salesforce. The adoption of any `Sales Admin` owned records will trigger an email alert that is sent to the Account Research Specialist for transparency and awareness of what account records have been claimed. 
2. The Account Research Specialist will be responsible for reviewing the `Prospect - CE User` accounts on a regular basis to determine additional account records that should be worked either by a Sales Team member or Outbound SDR.
3. When an account record has been identified for follow up, the Account Research Specialist will work with the appropriate Regional Director (RD) to determine Outbound SDR assignment based on work load and available capacity. 
4. The assigned Outbound SDR will work the `Prospect - CE User` account the same as any other known `CE User` account leveraging the tools at their disposal (DiscoverOrg, LinkedIn Sales Navigator, etc) to add contacts to the account record and populate the firmographic profile of the account.   



























[Return to Top of Page](#topofpage)