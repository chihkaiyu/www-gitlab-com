---
layout: markdown_page
title: "Operating Metrics"
---

## On this page
{:.no_toc}

- TOC
{:toc}

### Metrics Definitions and Review Process

We track a wide range of metrics on our corporate dashboard. Many definitions are self evident but some are not.

### Annual Recurring Revenue (ARR)

Recurring revenue recognized in current month multiplied by 12.

### Average Sales Price (ASP)

IACV per won deal.

### Bookings Total Contract Value (TCV)

All bookings in period (including multiyear); bookings is equal to billings with standard payment terms.

### Bookings Annual Contract Value (ACV)
Current Period subscription bookings which will result in revenue over next 12 months.

### Bookings Incremental Annual Contract Value (IACV)
Value of new bookings from new and existing customers that will result in recurring revenue over the next 12 months less any non-renewals, cancellations or any other decrease to annual recurring revenue. Excluded from IACV are bookings that are non-recurring such as professional services, training and non-recurring engineering fees (NRE). Also equals ACV less renewals.

### Retention, Net
Current period revenue from customers present 12 months prior divided by revenue from 12 months prior.

### Retention, Gross (Dollar weighted)
Remaining cohorts from Actual Subscription customers active as of 12 months ago multiplied by revenue from 12 months ago divided by actual Subscription customers as of date 12 months prior multiplied by revenue from 12 months ago. [Industry guidance]("http://www.forentrepreneurs.com/saas-metrics-2/") suggests median gross dollar churn performance for SaaS/subscription companies is 8% per year

### Customer Acquisition Cost (CAC)
Total Sales & Marketing Expense/Number of New Customers Acquired

### LTV to CAC Ratio
The Customer Lifetime Value to Customer Acquisition Ratio (LTV:CAC) measures the relationship between the lifetime value of a customer and the cost of acquiring that customer. A good LTV to CAC ratio is considered to be > 3.0.

### Rep Productivity
Current Month [Metric] /# of Reps on board for at least 90 days prior to start of period.

### Magic Number
IACV for trailing three months/Sales & Marketing Spend over trailing months -6 to months -4 (one quarter lag). [Industry guidance](http://www.thesaascfo.com/calculate-saas-magic-number/) suggests a good Magic Number is > 1.0.

### MQL
Marketing Qualified Lead

### SQL
Sales Qualified Lead

### Cost per MQL
Marketing expense divided by # MQLs

### Sales efficiency ratio
IACV / sales and marketing spend. [Industry guidance](http://tomtunguz.com/magic-numbers/) suggests that average performance is 0.8 with anything greater than 1.0 being considered very good.

### Marketing efficiency ratio
IACV / marketing spend

### Field efficiency ratio
IACV / sales spend

### LTV
Customer Lifetime Value = Average Revenue per Year x Gross Margin% x 1/(1-K) + GxK/(1-K)^2; K = (1-Net Churn) x (1-Discount Rate).  GitLab assumes a 10% cost of capital based on current cash usage and borrowing costs.

### Capital Consumption
TCV less Total Operating Expenses.  This metric tracks cash net cash consumed excluding changes in working capital.

### Monthly Metrics Review

Purpose:  We review all key operating metrics that either i) appear in the Corporate metrics sheet or ii) are included in the our Operating Model. The goal for the monthly meeting is to understand month to month variances as well as variances against the plan, forecast and operating model.

Agenda:
1. Review metric and conclude on implications for operating model.
1. Discuss proposals for different measurement.
1. Determine if external benchmarks are required.
1. Discuss proposals for addition of new metrics.
1. Discuss proposals for deprecation of existing metrics

Timing:  Meetings are monthly starting on the 10th day after month end.

Invitees:  Mandatory attendees are the owner of the metric and eteam functional representative and the CFO.  Optional attendees are the rest of the e-team and anyone who has an interest in the metric.

Process:  The owner of the metric(s) drives the meeting and is responsible for updating the doc with necessary data, links, MRs, etc. Calendar invites should be editable by invitees.  Use the CFO Zoom number.
