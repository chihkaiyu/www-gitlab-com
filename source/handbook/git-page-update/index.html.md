---
layout: markdown_page
title: "Edit this website locally"
---

## On this page
{:.no_toc}

- TOC
{:toc}

## Introduction

This is a guide on what you'll need to install and run on your machine so you can make edits locally. This is quicker than in the web interface and allows you a better overview and preview when making complex changes.

## 1. Help is available for team members

If you work for GitLab Inc. we don't expect you to figure this our by yourself.
If you have questions ask anyone for help, you're more likely to have success with:

- People that have been here longer than 3 months, the [team page](https://about.gitlab.com/team/) lists people in order of how long they have been here.
- People that have engineer in their title.
- We encourage you to meet someone new by asking someone your don't know for help. You can also ask for help in #questions or #peopleops by posting 'Who can do a video call with me to help me work on the website locally https://about.gitlab.com/handbook/git-page-update/ ?'. If the other options don't work for you please ask your buddy.

## 2. Start using GitLab

1. Here's where you can find step-by-step guides on the [basics of working with Git and GitLab](http://doc.gitlab.com/ce/gitlab-basics/README.html). You'll need those later.
1. Create your [SSH Keys](http://doc.gitlab.com/ce/gitlab-basics/create-your-ssh-keys.html).
1. For more information about using Git and GitLab see [GitLab University](https://docs.gitlab.com/ce/university/).

## 3. Install Git

1. Open a terminal.
1. Check your Git version by executing: `git --version`.
1. If Git is not installed, you should be prompted to install it. Follow this [guide](http://docs.gitlab.com/ce/gitlab-basics/start-using-git.html) to installing Git and
linking your account to Git.

## 4. Install RVM

1. Visit [https://rvm.io](https://rvm.io/).
1. In a terminal, execute: `curl -sSL https://get.rvm.io | bash -s stable`.
1. Close terminal.
1. Open a new terminal to load the new environment.

## 5. Install Ruby and Bundler

1. In a terminal, execute: `rvm install 2.3.3` to install Ruby
   (enter your system password if prompted).
1. Execute: `rvm use 2.3.3 --default` to set your default Ruby to `2.3.3`.
1. Execute: `ruby --version` to verify Ruby is installed. You should see:
   `ruby 2.3.3p222 (2016-11-21 revision 56859)`.
1. Execute: `gem install bundler` to install [Bundler](http://bundler.io/).

## 6. Clone the source of the website and install its dependencies

1. If you set up SSH keys previously, in terminal execute: `git clone git@gitlab.com:gitlab-com/www-gitlab-com.git` to clone the website. If you prefer using https, then execute: `git clone https://gitlab.com/gitlab-com/www-gitlab-com.git`, but note that if you've activated 2FA on your GitLab.com account, you'll need to take some additional steps to set up [personal access tokens](https://gitlab.com/profile/personal_access_tokens). If you ever want to switch between SSH and https, execute `git remote remove origin`, followed by `git remote add origin [..]` where the `[..]` is the part that starts with `git@` for SSH, or with `https:` for https.
1. Execute: `cd www-gitlab-com` to change to the `www-gitlab-com` directory.
1. Execute: `bundle install` to install all gem dependencies.

## 7. Prevent newlines from causing all following lines in a file to be tagged as changed

This is especially a problem for anyone running a Mac OSX operating system. The
command to 'tame' git is `git config --global core.autocrlf input` - execute it.

## 8. Preview website changes locally

1. In a terminal, execute: `bundle exec middleman`.
1. Visit http://localhost:4567 in your browser.
1. You will need to install a text editor to edit the site locally. We recommend
   [Sublime Text 3](http://www.sublimetext.com/3) or [Atom](https://atom.io/). Use command / ctrl P to quickly open the file you need.

## 9. Test if all URL links in a page are valid

Until this is automated in CI, a quick way to see if there are any invalid
links inside a page is the following.

1. Install the [check-my-links](https://chrome.google.com/webstore/detail/check-my-links/ojkcdipcgfaekbeaelaapakgnjflfglf/) extension in Chrome (no other browsers
   support unfortunately).
1. Open the page you wish to preview (see previous step).
1. Click the newly installed extension in the upper right corner of Chrome.

A pop-up window will open and tell you how many links, if any, are invalid.
Fix any invalid links and ideally any warnings, commit and push your changes,
and test again.

## 10. Start contributing

Most pages that you might want to edit are written in markdown [Kramdown](http://kramdown.gettalong.org/).
Read through our [Markdown Guide](/handbook/product/technical-writing/markdown-guide/) to understand its syntax and create new content.

Instructions on how to update the website are in the
[readme of www-gitlab-com](https://gitlab.com/gitlab-com/www-gitlab-com/blob/master/README.md).

## 11. Add yourself to the Team Page

We are happy to have you join our company and to include you in our [team page](https://about.gitlab.com/team/). Please follow the instructions and links below that will guide you through each step. There are instructions for both the web interface and locally, depending on your familiarity with git. Feel free to ask anyone at the company questions along the way.

### Add on GitLab.com (web interface)
   1. You should have received an invitation to the [www-gitLab-com project](https://gitlab.com/gitlab-com/www-gitlab-com) at your GitLab email account. After creating your account, take note of your username and password, because you will need them throughout these steps.
   2.  Find the picture that you'd like to add to our team page, change the picture's name to the following format: `yourname.jpg` or `yourname.png`. Ensure the picture size is around 400x400 (*it must be square*) and the format is JPEG or PNG. You can resize your picture using a photo editor like [GIMP](http://www.gimp.org/) (cross-platform) or online by searching for "image resize". Any picture that you provide will be made black-and-white automatically after you add it to the team page.
   3. In [GitLab.com](https://gitlab.com/), select the GitLab.com/www-gitlab-com project.
   4. Click on "Files".
   5. Select Source, Images, then Team.
   6. At the top of the page select “+” and upload the file. Note that your team page picture should be added to `www-gitlab-com/source/images/team/NAME-OF-PERSON-IN-LOWERCASE.jpg`.
   7. Commit these changes with a specific commit message “add NAME to team page” and a unique branch. Remember the branch name as you will use it in the following steps.
   8. [Create a merge request](http://doc.gitlab.com/ce/gitlab-basics/add-merge-request.html) in [GitLab.com](https://gitlab.com/) with the branch that you created with your picture.
   9. Find the dropdown menu at the top of your screen and find the branch that you previously created to add your picture (they are in alphabetical order).
   10. Go back to the GitLab.com project. Information displayed on Team page is pulled from a data file. You can find it by clicking on each of the following items: Files, `data/`, and then `team.yml`.
   11. When you are in `team.yml`, click on “edit” on the top right side of your screen.
   12. Your information should already be added after the last person on the team page. Update the initials to be your `Firstname 'Nickname' Lastname`. Verify that your title is entered correctly. Add the filename of the picture that you uploaded previously. Enter your Twitter and GitLab handle. Write a story about yourself. Don't forget to use other team members' information as a reference and to respect the spaces between lines. Please don't use "tab" because it will break the page format.
   13. After you added your information, add a comment to your commit and click on “Commit Changes”.
   14. Go to the Merge Request that you previously created with the branch that you are using and assign it to your manager for review.

### Add Locally (using the terminal)
   1. You should have received an invitation to the [www-gitLab-com project](https://gitlab.com/gitlab-com/www-gitlab-com) at your GitLab email account. After creating your account, take note of your username and password, because you will need them throughout these steps.
   2. Download Git, following the [start using git documentation](http://doc.gitlab.com/ce/gitlab-basics/start-using-git.html). Don't forget to add your Git username and to set your email.
   3. Follow the steps to create and add your [SSH keys](http://doc.gitlab.com/ce/gitlab-basics/create-your-ssh-keys.html). Note: in some of these steps, your [shell](http://doc.gitlab.com/ce/gitlab-basics/start-using-git.html) will require you to add your GitLab.com username and password.
   4. Clone the www-gitlab-com project through your shell, following the [command line commands documentation](http://doc.gitlab.com/ce/gitlab-basics/command-line-commands.html).
   5. Create and checkout a new branch for the changes you will be making.
   6. Find the picture that you’d like to add to our team page, change the picture's name to the following format: `yourname.jpg` or `yourname.png` and then add it to the `source/images/team/` directory. Follow the "[how to add an image](http://doc.gitlab.com/ce/gitlab-basics/add-image.html)" steps. Ensure the picture size is around 400x400 (it must be square) and the format is JPEG or PNG. You can resize your picture using a photo editor like [GIMP](http://www.gimp.org/) (cross-platform) or online by searching for "image resize". Any picture that you provide will be made black-and-white automatically after you add it to the team page.
   7. Add the picture so it is staged for commit.
   8. Information displayed on the Team page is pulled from `data/team.yml`.
   9. Your information should already be added after the last person on the team page. Update the initials to be your `Firstname 'Nickname' Lastname`. Verify that your title is entered correctly. Add the file name of the picture that you uploaded previously. Enter your twitter and GitLab handle. Write a story about yourself. Don't forget to use other team members' information as a reference and to respect the spaces between lines. Please don't use "tab" because it will break the page format.
   10. After you added your information and saved your changes, add the file to be staged for commit.
   11. To see your changes locally, follow the directions in `README.md`.
   12. After validating your changes, commit your changes with a comment and push your branch.
   13. [Create a Merge Request](http://doc.gitlab.com/ce/gitlab-basics/add-merge-request.html) in [GitLab.com](https://gitlab.com/) with the branch that you created. Assign it to your manager for review.
